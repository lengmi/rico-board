
硬件        |型号            | 功能          |备注
------------|----------------|--------------|------
电源        |TPS65218        |为am4378供电   |I2C0作为配置口
主芯片      |am4378BZD       |核心处理器      |与TI参考板相同
晶振        |24MHz           |时钟源         |OSC0
实时晶振     |32.768kHz      |RTC            |OSC1
外部ram     |MT41K128M16HA   |外部随机存储器  |2个256M ddr3
板上emmc    |KLM4G1YEMD-B031 |4G eMMC存储    |对应am4378的MMC1
16M QSPI闪存 |S25FL128SAGNFI |板上flash存储器|
HDMI        |SIL9022ACNU     |并行lcd转HDMI |
以太网接口   |KSZ9031RNXCA    |              |
Mini USB    |                | 小口usb      |USB1
USB HOST    |                | 大口usb      |USB0
板卡信息存储 |CAT24C256W      |             |I2C0口作为通信
J12         |                |摄像头排线    |CAM0+I2C0
J13         |                |摄像头排线    |CAM1+I2C1

*注：TI评估板用的是4个8位512M的ddr3.而rico-board用的是2个16位256M的ddr3*
